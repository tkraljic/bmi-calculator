#include <iostream>

int main()
{
    float weightInPounds, heightInInches, bmi;
    std::string sex, firstName, lastName;

    std::cout << "BMI Calculator" << std::endl;
    std::cout << "\nUnderweight: BMI < 18.5%" << std::endl;
    std::cout << "Normal: BMI > 18.5% and < 24.9%" << std::endl;
    std::cout << "Overweight: BMI > 29.9%" << std::endl;
    std::cout << "Obese: BMI > 30\n" << std::endl;

    std::cout << "\nEnter your first name:" << std::endl;
    std::cin >> firstName;

    std::cout << "\nEnter your last name: " << std::endl;
    std::cin >> lastName;

    std::cout << "\nWeight(ibs): " << std::endl;
    std::cin >> weightInPounds;

    std::cout << "\nHeight(inches): " << std::endl;
    std::cin >> heightInInches;

    bmi = (weightInPounds * 703) / pow(heightInInches,2); //

    if (bmi < 18.5 ) {
        std::cout  << "\n" << firstName << " " << lastName << ',' << "your BMI is " << bmi << "%" << std::endl;
        std::cout << "\nBased on your BMI, you are considered underweight." << std::endl;
    }
    else if (bmi >= 18.5 && bmi < 24.9) {
        std::cout  << "\n" << firstName << " " << lastName << ',' << "your BMI is " << bmi << "%" << std::endl;
        std::cout << "\nBased on your BMI, you are at a normal weight." << std::endl;
    }
    else if (bmi > 25) {
        std::cout  << "\n" << firstName << " " << lastName << ',' << "your BMI is " << bmi << "%" << std::endl;
        std::cout << "\nBased on your BMI, you are considered overweight." << std::endl;
    }
    else
        std::cout << "Invalid input." << std::endl;

    return 0;
}